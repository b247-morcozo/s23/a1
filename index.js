/*3. Create a trainer object using object literals.
4. Initialize/add the following trainer object properties:
Name (String)
Age (Number)
Pokemon (Array)
Friends (Object with Array values for properties)
5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
6. Access the trainer object properties using dot and square bracket notation.
7. Invoke/call the trainer talk object method.
*/

			let trainer = {
				Name: 'Ash Ketchum',
				Age: 10,
				Pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
				Friends: {
					Hoenn: ["May", "Max"],
					Kanto: ["Brock", "Misty"],
				},
				talk: function(){
					console.log("Pikachu! I choose you!");
				}
			};

			console.log(trainer);
			console.log("Result of dot notation: ");
			console.log(trainer.Name);
			console.log("Result of square bracket notation: ")
			console.log(trainer["Pokemon"]);
			console.log("Result of talk method: ");
			trainer.talk();

/*8. Create a constructor for creating a pokemon with the following properties:
Name (Provided as an argument to the contructor)
Level (Provided as an argument to the contructor)
Health (Create an equation that uses the level property)
Attack (Create an equation that uses the level property)
9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.*/


			class pokemon {
				constructor(name, level){
					this.name = name;
					this.level = level;
					this.health = 2 * level;
					this.attack = level;

					this.tackle = function(target){
					console.log(this.name + 'tackled' + target.name);
					console.log(target.name + "'s health is now reduced to " + Number(target.health - this.attack));
					target.health = target.health - this.attack
					console.log(target)
					if (target.health <= 0) {
						target.faint()
					}
				};

				this.faint = function(){
					console.log(this.name + ' fainted. ');
				};
				}
			}

			let pikachu = new pokemon('Pikachu', 12);
			let geodude = new pokemon('Geodude', 8);
			let mewtwo = new pokemon('Mewtwo', 100);

			console.log(pikachu);
			console.log(geodude);
			console.log(mewtwo);


			geodude.tackle(pikachu);
			mewtwo.tackle(geodude);
			